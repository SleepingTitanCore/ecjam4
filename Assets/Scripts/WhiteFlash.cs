﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WhiteFlash : MonoBehaviour
{
    public UnityEvent onFlashFull;
    private Animator animator;
    public AudioSource audioSrc { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        audioSrc = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
    }

    public void OnFlash()
    {
        onFlashFull.Invoke();
    }

    public void Flash()
    {
        animator.SetTrigger("flash");
        audioSrc.Play();
    }
}
