﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeValueChange : MonoBehaviour
{
    // Reference to Audio Source component
    private AudioSource audioSrc;

    // Use this for initialization
    void Start()
    {
        // Assign Audio Source component to control it
        audioSrc = GetComponent<AudioSource>();
        GetComponentInChildren<Slider>().SetValueWithoutNotify(AudioListener.volume);
    }

    // Method that is called by slider game object
    // This method takes vol value passed by slider
    // and sets it as musicValue
    public void SetVolume(float vol)
    {
        AudioListener.volume = vol;
        if (!audioSrc.isPlaying)
        {
            audioSrc.Play();
        }
    }
}
