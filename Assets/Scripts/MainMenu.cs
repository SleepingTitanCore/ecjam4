﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public string levelToLoad = "";

    public void PlayGame ()
    {
        if(levelToLoad == "")
        {
            Debug.Log("Wow, let's load the next scene in the build index boiii");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else
        {
            Debug.Log("Trying to load scene " + levelToLoad);
            SceneManager.LoadScene(levelToLoad);
        }

        levelToLoad = "";
    }

    public void QuitGame ()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }

    public void AdjustVolume(float newVolume)
    {
        AudioListener.volume = newVolume;
    }
}
