﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastDialogScene : MonoBehaviour
{
    public string levelToLoad;
    public TextBox textBox;
    public Dialogue[] lastDialog;

    // Start is called before the first frame update
    void Start()
    {
        textBox.onDialogueFinish.AddListener(() => CircleTransition.Instance.LoadScene(levelToLoad));
        textBox.PlayDialogue(lastDialog);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
