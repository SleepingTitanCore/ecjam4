﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Ghost : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [Tooltip("Name of this ghost.")]
    public string ghostName;

    [TextArea(5, 10)]
    public string blurb;

    [Range(0f, 1f)]
    public float animationStartTime;

    public Image ghostBody;

    public Image highlightImage;

    public TextBox textBox;

    public float lerpSpeed = 15;

    public static Ghost selectedGhost;
    public static Ghost hightlightedGhost;
    public Color highlightColor { get; private set; }
    public Animator animator { get; private set; }

    private static Ghost lastHighlightedGhost;
    private RectTransform rectTransform;
    private RectTransform savedRectTransform;
    private Vector3 cachedPosition; // caching position in case we let go of mouse
    private AudioSource audioSrc;

    // Start is called before the first frame update
    void Awake()
    {
        audioSrc = GetComponent<AudioSource>();
        rectTransform = GetComponent<RectTransform>();
        GameObject obj = new GameObject(name + "savedAnchorPosition");
        obj.transform.parent = rectTransform.parent;

        savedRectTransform = obj.AddComponent<RectTransform>();
        savedRectTransform.anchorMax = rectTransform.anchorMax;
        savedRectTransform.anchorMin = rectTransform.anchorMin;
        savedRectTransform.sizeDelta = rectTransform.sizeDelta;
        savedRectTransform.position = rectTransform.position;
        savedRectTransform.anchoredPosition = rectTransform.anchoredPosition;

        animator = GetComponent<Animator>();
        animator.Play("GhostFloat", 0, animationStartTime);
        highlightImage.enabled = false;
        cachedPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        highlightColor = new Color(highlightImage.color.r, highlightImage.color.g, highlightImage.color.b, highlightImage.color.a);
    }

    public void Update()
    {
        if(selectedGhost != this)
        {
            rectTransform.position = Vector3.Lerp(rectTransform.position, savedRectTransform.position, Time.deltaTime * lerpSpeed);
        }
    }

    private void OnEnable()
    {
        animator.Play("GhostFloat", 0, animationStartTime);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (selectedGhost) return;

        if(!(lastHighlightedGhost == this))
        {
            textBox.SetText(ghostName, blurb);
        }

        audioSrc.Play();
        highlightImage.enabled = true;
        hightlightedGhost = this;
        lastHighlightedGhost = this;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (this == selectedGhost) return;
        Unhighlight();
    }

    public void Unhighlight()
    {
        hightlightedGhost = null;
        highlightImage.enabled = false;
    }

    public void ResetState()
    {
        rectTransform.localScale = new Vector3(1,1,1);
        rectTransform.position = new Vector3(savedRectTransform.position.x, savedRectTransform.position.y + 500, savedRectTransform.position.z);
        highlightImage.color = highlightColor;
    }
}
