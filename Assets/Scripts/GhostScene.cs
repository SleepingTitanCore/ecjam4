﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostScene : MonoBehaviour
{
    [Serializable]
    public class GhostData
    {
        public Sprite ghostSprite;
        public Sprite ghostOutcome;

        public string ghostName;

        [TextArea(2, 25)]
        public string ghostBlurb;

        public AudioClip ghostOutcomeSound;
        public Dialogue[] ghostExitDialouge;
    }

    public Sprite sceneSprite;

    public Dialogue[] sceneIntroDialogue;

    public GhostData[] ghostData = new GhostData[3];

    [Header("Next Scene")]
    public GhostScene nextScene;
}
