﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.Events;
using UnityEngine.UI;

public class TextBox : MonoBehaviour
{
    public Color32 textColor;
    public float namePlaySpeed;
    public float blurbPlaySpeed;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI blurbText;
    public GameObject continueText;
    public Image mouseBlockingImage; // Used for intercepting mouse pointer enter/exit calls (mainly for dialogue playing)
    public UnityEvent onDialogueFinish;

    private AudioSource dialogeAudioSrc;

    // Start is called before the first frame update
    void Awake()
    {
        dialogeAudioSrc = GetComponent<AudioSource>();
        blurbText = GetComponentInChildren<TextMeshProUGUI>();
    }

    public void PlayDialogue(Dialogue[] text)
    {
        StopAllCoroutines();
        StartCoroutine(PlayDialog(text));
    }

    public void SetText(string ghostName, string ghostBlurb)
    {
        // Debug.Log("setting text");
        StopAllCoroutines();
        nameText.text = ghostName;
        blurbText.text = ghostBlurb;
        StartCoroutine(ShowText());
        StartCoroutine(ShowName());
    }

    IEnumerator PlayDialog(Dialogue[] dialogues)
    {
        mouseBlockingImage.enabled = true;
        for(int dialogueIndex = 0; dialogueIndex < dialogues.Length; ++dialogueIndex)
        {

            // Set name if it isn't the same as before.
            if(nameText.text != dialogues[dialogueIndex].charName)
            {
                StopCoroutine(ShowName());
                nameText.text = dialogues[dialogueIndex].charName;
                StartCoroutine(ShowName());
            }

            // Play dialogue Audio
            dialogeAudioSrc.Play();

            continueText.SetActive(false);
            blurbText.text = dialogues[dialogueIndex].blurb;
            blurbText.maxVisibleCharacters = 0;
            for (int i = 0; i <= dialogues[dialogueIndex].blurb.Length; i++)
            {
                blurbText.maxVisibleCharacters = i;
                yield return new WaitForSecondsRealtime(1 / blurbPlaySpeed);
            }
            continueText.SetActive(true);

            while(!Input.GetKey(KeyCode.E) && !Input.GetMouseButton(0))
            {
                yield return null;
            }
        }
        mouseBlockingImage.enabled = false;
        continueText.SetActive(false);
        onDialogueFinish.Invoke();
        onDialogueFinish.RemoveAllListeners();
    }

    IEnumerator ShowText()
    {
        for (int i = 0; i <= blurbText.textInfo.characterCount; i++)
        {
            blurbText.maxVisibleCharacters = i;
            yield return new WaitForSecondsRealtime(1 / blurbPlaySpeed);
        }
    }

    IEnumerator ShowName()
    {
        for (int i = 0; i <= nameText.textInfo.characterCount; i++)
        {
            nameText.maxVisibleCharacters = i;
            yield return new WaitForSecondsRealtime(1 / namePlaySpeed);
        }
    }

    public void ClearText()
    {
        nameText.text = "";
        blurbText.text = "";
    }

}
