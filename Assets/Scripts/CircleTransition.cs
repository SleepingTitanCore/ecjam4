﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleTransition : MonoBehaviour
{
    public static CircleTransition Instance;
    private MainMenu mainMenu;
    private Animator animator;

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            mainMenu = GetComponent<MainMenu>();
            animator = GetComponent<Animator>();
        }
    }

    public void LoadSetScene()
    {
        animator.SetTrigger("out");
    }

    public void LoadScene(string sceneName)
    {
        mainMenu.levelToLoad = sceneName;
        LoadSetScene();
    }

    private void OnOut()
    {
        mainMenu.PlayGame();
    }
}
