﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GhostManager : MonoBehaviour
{
    public Ghost ghostOne;
    public Ghost ghostTwo;
    public Ghost ghostThree;
    public TextBox textBox;
    public float mouseLerpSpeed = 5;
    public Image sceneImage;
    public WhiteFlash screenFlash;
    public AudioSource ghostPickedAudioSource;

    public GhostScene ghostScene;

    private bool selectedGhostIsInBottom = false;
    private Sprite sceneToSwitchTo;
    private AudioSource sceneSwapAudioSrc;

    private void Start()
    {
        sceneSwapAudioSrc = GetComponent<AudioSource>();

        SetGhostData(ghostOne, ghostScene.ghostData[0]);
        SetGhostData(ghostTwo, ghostScene.ghostData[1]);
        SetGhostData(ghostThree, ghostScene.ghostData[2]);
        SetGhostsEnabled(false);
        screenFlash.onFlashFull.AddListener(() => SetScene(sceneToSwitchTo));
        textBox.onDialogueFinish.AddListener(() => { SetGhostsEnabled(true);  ResetGhostPositions(); textBox.ClearText(); });
        textBox.PlayDialogue(ghostScene.sceneIntroDialogue);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && Ghost.hightlightedGhost)
        {
            Ghost.selectedGhost = Ghost.hightlightedGhost;
            // Debug.Log("Set ghost to " + Ghost.hightlightedGhost);
        }
        if(Ghost.selectedGhost)
        {
            if(Input.mousePosition.y < Screen.height / 2 && Input.mousePosition.y > 0 &&
                    Input.mousePosition.x > 0 && Input.mousePosition.x < Screen.width)
            {
                if(!selectedGhostIsInBottom)
                {
                    selectedGhostIsInBottom = true;
                    Ghost.selectedGhost.highlightImage.color = new Color(0, 1, 0, 0.5f);
                }
            }
            else if(selectedGhostIsInBottom)
            {
                selectedGhostIsInBottom = false;
                Ghost.selectedGhost.highlightImage.color = Ghost.selectedGhost.highlightColor;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (Ghost.selectedGhost)
            {
                // Test and see if we've successfully picked a ghost by checking if we're in the bottom half of our screen
                // (but not outside of it).
                if(Input.mousePosition.y < Screen.height / 2 && Input.mousePosition.y > 0 && 
                    Input.mousePosition.x > 0 && Input.mousePosition.x < Screen.width)
                {
                    GhostScene.GhostData selectedGhostData = GetGhostDataFromSelected();
                    Debug.Log("We picked " + Ghost.selectedGhost);
                    sceneToSwitchTo = selectedGhostData.ghostOutcome;

                    // Load our next scenario when our exit dialog finishes.
                    if(ghostScene.nextScene)
                    {
                        textBox.onDialogueFinish.AddListener(() =>
                        {
                            GetComponent<Animator>().SetTrigger("loadnextscene");
                            textBox.mouseBlockingImage.enabled = true;
                            sceneSwapAudioSrc.Play();
                        });
                    }
                    else
                    {
                        // We hit the last scene. Just transition to the last level
                        textBox.onDialogueFinish.AddListener(() =>
                        {
                            CircleTransition.Instance.LoadSetScene();
                        });
                    }

                    // Play the dialog
                    textBox.PlayDialogue(selectedGhostData.ghostExitDialouge);
                    // Flash our outcome
                    screenFlash.Flash();
                    ghostPickedAudioSource.clip = selectedGhostData.ghostOutcomeSound;
                    ghostPickedAudioSource.Play();
                    // Disable the spirit that we selected
                    Ghost.selectedGhost.gameObject.SetActive(false);
                }

                // Reset the ghost since it wasn't actually picked for the scenario.
                Ghost.selectedGhost.ResetState();
                Ghost.selectedGhost.Unhighlight();
            }

            Ghost.selectedGhost = null;
        }
        if (Ghost.selectedGhost)
        {
            Ghost.selectedGhost.transform.position = Vector3.Lerp(Ghost.selectedGhost.transform.position, 
                Input.mousePosition, mouseLerpSpeed * Time.deltaTime);
        }
    }

    private void SetScene(Sprite sceneSprite)
    {
        sceneImage.sprite = sceneToSwitchTo;
    }

    private void ResetGhostPositions()
    {
        GetComponent<Animator>().Play("Idle", 0);
        ghostOne.ResetState();
        ghostTwo.ResetState();
        ghostThree.ResetState();
    }

    private void SetGhostsEnabled(bool enabled)
    {
        ghostOne.gameObject.SetActive(enabled);
        ghostTwo.gameObject.SetActive(enabled);
        ghostThree.gameObject.SetActive(enabled);
    }

    private GhostScene.GhostData GetGhostDataFromSelected()
    {
        if (Ghost.selectedGhost == ghostOne)
        {
            return ghostScene.ghostData[0];
        }
        else if (Ghost.selectedGhost == ghostTwo)
        {
            return ghostScene.ghostData[1];
        }
        else
        {
            return ghostScene.ghostData[2];
        }
    }

    private void LoadNextGhostScene()
    {
        textBox.mouseBlockingImage.enabled = false;
        ghostScene = ghostScene.nextScene;
        sceneImage.sprite = ghostScene.sceneSprite;
        SetGhostData(ghostOne, ghostScene.ghostData[0]);
        SetGhostData(ghostTwo, ghostScene.ghostData[1]);
        SetGhostData(ghostThree, ghostScene.ghostData[2]);
        SetGhostsEnabled(false);
        textBox.onDialogueFinish.AddListener(() => {
            SetGhostsEnabled(true); textBox.ClearText(); ResetGhostPositions();
        });
        textBox.PlayDialogue(ghostScene.sceneIntroDialogue);
    }

    private void SetGhostData(Ghost ghost, GhostScene.GhostData data)
    {
        // Debug.Log("Setting name of " + ghost + " to " + data.ghostName + " with blurb " + data.ghostBlurb);
        ghost.ghostName = data.ghostName;
        ghost.blurb = data.ghostBlurb;
        ghost.ghostBody.sprite = null;
        ghost.ghostBody.sprite = data.ghostSprite;
    }

}
