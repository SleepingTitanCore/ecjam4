﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameJamWebLoader : MonoBehaviour
{
    public void LoadJamSite()
    {
        Application.OpenURL("https://itch.io/jam/extra-credits-game-jam-4");
    }
}
